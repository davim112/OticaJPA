/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.projeto.sisotica.model;

/**
 *
 * @author Davi Martins
 */
public enum Categoria {
    
    ARMACOES,
    LENTES
}
