/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.projeto.sisotica.controllers.listagem;

import com.projeto.sisotica.controllers.cadastro.CadastroProdutoController;
import com.projeto.sisotica.controllers.AbstractController;
import com.projeto.sisotica.dao.imp.ProdutoDao;
import com.projeto.sisotica.model.Produto;
import com.projeto.sisotica.view.CadastroProdutoForm;
import com.projeto.sisotica.view.ListagemProdutoForm;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Davi Martins
 * @author Vinicius Fraga
 */
public class ListagemProdutoController extends AbstractController<Produto> {

    private ListagemProdutoForm view;
    private List<Produto> lista;
    private ProdutoDao produtoDao;

    public ListagemProdutoController() {
        view = new ListagemProdutoForm();

        produtoDao = new ProdutoDao();
        lista = produtoDao.findAll(Produto.class);

        String[] colunas = {"nome", "preço", "descricao"};
        DefaultTableModel model = new DefaultTableModel(colunas, 0);

        for (Produto p : lista) {
            model.addRow(new Object[] {
                    p.getNome(),
                    p.getPreco(),
                    p.getDescricao()
            });
        }
        
        view.getTblProdutos().setModel(model);
        
         if(view.getTblProdutos().getRowCount() > 0)
            view.getTblProdutos().changeSelection(0, 0, false, false);
        
        view.getBtnVisualizar().addActionListener((ActionEvent e) -> {
            new CadastroProdutoController(lista.get(view.getTblProdutos().getSelectedRow()));
            view.dispose();
        });

        view.getBtnSair().addActionListener((ActionEvent e) -> {
            view.dispose();
        });
    }

}
