/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.projeto.sisotica.dao.imp;

import com.projeto.sisotica.dao.GenericDao;
import com.projeto.sisotica.model.Venda;

/**
 *
 * @author Davi Martins
 * @author Vinicius Fraga
 */
public class VendaDao extends GenericDao<Venda>{
    
    public VendaDao(){
        super();
    }
}
